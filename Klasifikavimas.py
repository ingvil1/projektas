
# coding: utf-8

# In[1]:

get_ipython().system('pip install findspark')


# In[2]:

import csv


# In[3]:

import pyspark


# In[4]:

import findspark
import os
import csv
from pyspark import ml
from pyspark import mllib
import pyspark.ml.feature
import pyspark.ml.clustering
import pyspark.ml.classification
import pyspark.ml.evaluation
import pyspark.mllib.feature
import pyspark.mllib.clustering
import ggplot as gg
import pandas as pd
import numpy as np


# In[5]:

findspark.init()


# In[6]:

os.makedirs(os.path.join(os.getcwd(), "output"), exist_ok=True)


# In[7]:

merge = sqlContext.read.load("merge.csv", format="com.databricks.spark.csv",
header=True, inferSchema=True).select(
["user_lifetime", "user_no_outgoing_activity_in_days",
"user_account_balance_last", "user_spendings", "reloads_inactive_days",
"reloads_count", "reloads_sum", "calls_outgoing_count","calls_outgoing_spendings", "calls_outgoing_duration",
"calls_outgoing_spendings_max", "calls_outgoing_duration_max",
"calls_outgoing_inactive_days", "calls_outgoing_to_onnet_count",
"calls_outgoing_to_onnet_spendings", "calls_outgoing_to_onnet_duration",
"calls_outgoing_to_onnet_inactive_days", "calls_outgoing_to_offnet_count",
"calls_outgoing_to_offnet_spendings", "calls_outgoing_to_offnet_duration",
"calls_outgoing_to_offnet_inactive_days", "calls_outgoing_to_abroad_count",
"calls_outgoing_to_abroad_spendings", "calls_outgoing_to_abroad_duration",
"calls_outgoing_to_abroad_inactive_days", "sms_outgoing_count",
"sms_outgoing_spendings", "sms_outgoing_spendings_max",
"sms_outgoing_inactive_days", "sms_outgoing_to_onnet_count",
"sms_outgoing_to_onnet_spendings", "sms_outgoing_to_onnet_inactive_days",
"sms_outgoing_to_offnet_count", "sms_outgoing_to_offnet_spendings",
"sms_outgoing_to_offnet_inactive_days", "sms_outgoing_to_abroad_count",
"sms_outgoing_to_abroad_spendings", "sms_outgoing_to_abroad_inactive_days",
"sms_incoming_count", "sms_incoming_spendings",
"sms_incoming_from_abroad_count", "sms_incoming_from_abroad_spendings",
"gprs_session_count", "gprs_usage", "gprs_spendings", "gprs_inactive_days",
"last_100_reloads_count", "last_100_reloads_sum",
"last_100_calls_outgoing_duration",
"last_100_calls_outgoing_to_onnet_duration",
"last_100_calls_outgoing_to_offnet_duration",
"last_100_calls_outgoing_to_abroad_duration", "last_100_sms_outgoing_count",
"last_100_sms_outgoing_to_onnet_count",
"last_100_sms_outgoing_to_offnet_count",
"last_100_sms_outgoing_to_abroad_count", "last_100_gprs_usage", "user_intake",
"user_has_outgoing_calls", "user_has_outgoing_sms", "user_use_gprs",
"user_does_reload", "n_months", "churn"]
)


# In[8]:

merge.show(5)


# In[9]:

from pyspark.ml.feature import VectorAssembler


# In[10]:

assembler = VectorAssembler(inputCols=["user_lifetime",
"user_no_outgoing_activity_in_days", "user_account_balance_last",
"user_spendings", "reloads_inactive_days", "reloads_count", "reloads_sum",
"calls_outgoing_count", "calls_outgoing_spendings", "calls_outgoing_duration",
"calls_outgoing_spendings_max", "calls_outgoing_duration_max",
"calls_outgoing_inactive_days", "calls_outgoing_to_onnet_count","calls_outgoing_to_onnet_spendings", "calls_outgoing_to_onnet_duration",
"calls_outgoing_to_onnet_inactive_days", "calls_outgoing_to_offnet_count",
"calls_outgoing_to_offnet_spendings", "calls_outgoing_to_offnet_duration",
"calls_outgoing_to_offnet_inactive_days", "calls_outgoing_to_abroad_count",
"calls_outgoing_to_abroad_spendings", "calls_outgoing_to_abroad_duration",
"calls_outgoing_to_abroad_inactive_days", "sms_outgoing_count",
"sms_outgoing_spendings", "sms_outgoing_spendings_max",
"sms_outgoing_inactive_days", "sms_outgoing_to_onnet_count",
"sms_outgoing_to_onnet_spendings", "sms_outgoing_to_onnet_inactive_days",
"sms_outgoing_to_offnet_count", "sms_outgoing_to_offnet_spendings",
"sms_outgoing_to_offnet_inactive_days", "sms_outgoing_to_abroad_count",
"sms_outgoing_to_abroad_spendings", "sms_outgoing_to_abroad_inactive_days",
"sms_incoming_count", "sms_incoming_spendings",
"sms_incoming_from_abroad_count", "sms_incoming_from_abroad_spendings",
"gprs_session_count", "gprs_usage", "gprs_spendings", "gprs_inactive_days",
"last_100_reloads_count", "last_100_reloads_sum",
"last_100_calls_outgoing_duration",
"last_100_calls_outgoing_to_onnet_duration",
"last_100_calls_outgoing_to_offnet_duration",
"last_100_calls_outgoing_to_abroad_duration", "last_100_sms_outgoing_count",
"last_100_sms_outgoing_to_onnet_count",
"last_100_sms_outgoing_to_offnet_count",
"last_100_sms_outgoing_to_abroad_count", "last_100_gprs_usage", "user_intake",
"user_has_outgoing_calls", "user_has_outgoing_sms", "user_use_gprs",
"user_does_reload"], outputCol="features")


# In[11]:

dataFeaturizedDF = assembler.transform(merge)
dataFeaturizedDF.show(5)


# In[12]:

dataFeaturizedDF.first()


# In[13]:

from pyspark.mllib.linalg import DenseVector


# In[14]:

merge.rdd.take(5)


# In[15]:

featuresRDD = merge.rdd.map(lambda row: DenseVector([row.user_lifetime,
row.user_no_outgoing_activity_in_days, row.user_account_balance_last,row.user_spendings, row.reloads_inactive_days, row.reloads_count,
row.reloads_sum, row.calls_outgoing_count, row.calls_outgoing_spendings,
row.calls_outgoing_duration, row.calls_outgoing_spendings_max,
row.calls_outgoing_duration_max, row.calls_outgoing_inactive_days,
row.calls_outgoing_to_onnet_count, row.calls_outgoing_to_onnet_spendings,
row.calls_outgoing_to_onnet_duration,
row.calls_outgoing_to_onnet_inactive_days, row.calls_outgoing_to_offnet_count,
row.calls_outgoing_to_offnet_spendings, row.calls_outgoing_to_offnet_duration,
row.calls_outgoing_to_offnet_inactive_days,
row.calls_outgoing_to_abroad_count, row.calls_outgoing_to_abroad_spendings,
row.calls_outgoing_to_abroad_duration,
row.calls_outgoing_to_abroad_inactive_days, row.sms_outgoing_count,
row.sms_outgoing_spendings, row.sms_outgoing_spendings_max,
row.sms_outgoing_inactive_days, row.sms_outgoing_to_onnet_count,
row.sms_outgoing_to_onnet_spendings, row.sms_outgoing_to_onnet_inactive_days,
row.sms_outgoing_to_offnet_count, row.sms_outgoing_to_offnet_spendings,
row.sms_outgoing_to_offnet_inactive_days, row.sms_outgoing_to_abroad_count,
row.sms_outgoing_to_abroad_spendings,
row.sms_outgoing_to_abroad_inactive_days, row.sms_incoming_count,
row.sms_incoming_spendings, row.sms_incoming_from_abroad_count,
row.sms_incoming_from_abroad_spendings, row.gprs_session_count,
row.gprs_usage, row.gprs_spendings, row.gprs_inactive_days,
row.last_100_reloads_count, row.last_100_reloads_sum,
row.last_100_calls_outgoing_duration,
row.last_100_calls_outgoing_to_onnet_duration,
row.last_100_calls_outgoing_to_offnet_duration,
row.last_100_calls_outgoing_to_abroad_duration,
row.last_100_sms_outgoing_count, row.last_100_sms_outgoing_to_onnet_count,
row.last_100_sms_outgoing_to_offnet_count,
row.last_100_sms_outgoing_to_abroad_count, row.last_100_gprs_usage,
row.user_intake, row.user_has_outgoing_calls, row.user_has_outgoing_sms,
row.user_use_gprs, row.user_does_reload]))


# In[16]:

featuresRDD.take(5)


# In[17]:

churn = merge.map(lambda row: row[-1])
churn.take(5)


# In[19]:

transformedData = churn.zip(featuresRDD)
transformedData.take(5)


# In[20]:

from pyspark.mllib.regression import LabeledPoint


# In[21]:

temp = merge.map(lambda line:LabeledPoint(line[-1],[line[:63]]))
temp.take(5)


# In[22]:

from pyspark.mllib.tree import DecisionTree, DecisionTreeModel
from pyspark.mllib.util import MLUtils


# In[23]:

model = DecisionTree.trainClassifier(temp, numClasses=2,
categoricalFeaturesInfo={},impurity='gini', maxDepth=5, maxBins=32)


# In[24]:

(trainingDF, testDF) = temp.randomSplit([0.7, 0.3])


# In[25]:

model = DecisionTree.trainClassifier(trainingDF, numClasses=2,
categoricalFeaturesInfo={},impurity='gini', maxDepth=5, maxBins=32)


# In[26]:

predictions = model.predict(testDF.map(lambda x: x.features))


# In[27]:

labelsAndPredictions = testDF.map(lambda lp: lp.label).zip(predictions)


# In[28]:

testErr = labelsAndPredictions.filter(lambda seq: seq[0] != seq[1]).count()/float(testDF.count())


# In[29]:

print('Test Error = ' + str(testErr))


# In[30]:

print('Learned classification tree model:')


# In[31]:

print(model.toDebugString())

