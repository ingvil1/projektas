
# coding: utf-8

# In[1]:

get_ipython().system('pip install findspark')


# In[2]:

import csv


# In[3]:

import pyspark


# In[4]:

from pyspark import ml
from pyspark import mllib
import pyspark.ml.feature
import pyspark.ml.clustering
import pyspark.ml.classification
import pyspark.ml.evaluation
import pyspark.mllib.feature
import pyspark.mllib.clustering
import ggplot as gg
import pandas as pd


# In[5]:

get_ipython().magic('matplotlib inline')


# In[6]:

with open("output/header__aggregated_final.txt") as f:
    columnsAggfinal = f.read().split(",")

aggfinal = (
    sqlContext.read.format("com.databricks.spark.csv")
    .options(inferSchema=True)
    .load("output/aggregated_final/")
    .rdd
    .toDF(columnsAggfinal)
)


# In[7]:

aggfinal


# In[8]:

aggfinal.first()


# In[9]:

import os

import numpy as np

from pyspark.ml.feature import VectorAssembler, StandardScaler
from pyspark.mllib.linalg import DenseVector, Vectors
def ensureDenseFeatures(featurizedDF, featuresCol="features"):
    columns = featurizedDF.columns
    idxCol = columns.index(featuresCol)
    transformedRow = pyspark.sql.Row(*columns)
    
    def rowColumnSparseToDense(row):
        values = list(row)
        values[idxCol] = pyspark.mllib.linalg.DenseVector(row[idxCol].toArray())
        return transformedRow(*values)
    
    featurizedDF = featurizedDF.rdd.map(rowColumnSparseToDense).toDF()
    
    return featurizedDF


# In[10]:

def ensureDenseFeatures(featurizedDF, featuresCol="features"):
    columns = featurizedDF.columns
    idxCol = columns.index(featuresCol)
    transformedRow = pyspark.sql.Row(*columns)
    
    def rowColumnSparseToDense(row):
        values = list(row)
        if isinstance(values[idxCol], DenseVector):
            return row
        values[idxCol] = pyspark.mllib.linalg.DenseVector(row[idxCol].toArray())
        return transformedRow(*values)
    
    featurizedDF = featurizedDF.rdd.map(rowColumnSparseToDense).toDF()
    
    return featurizedDF


# In[11]:

with open("output/columns_continuous__agg_final.txt") as f:
    continouosColumns = f.read().split(",")
    
with open("output/columns_binary__agg_final.txt") as f:
    binaryColumns = f.read().split(",")
    
with open("output/columns_ids__agg_final.txt") as f:
    idsColumns = f.read().split(",")

with open("output/columns_misc__agg_final.txt") as f:
    miscColumns = f.read().split(",")


# In[12]:

ls output/


# In[13]:

assembler = VectorAssembler(inputCols=continouosColumns, outputCol="features")


# In[14]:

featurizedDF = assembler.transform(aggfinal)


# In[15]:

featurizedDF.first()


# In[16]:

scaler = StandardScaler(withMean=True, withStd=True, inputCol="features", outputCol="scaled_features")


# In[17]:

scalerModelOrg = scaler.fit(featurizedDF)


# In[18]:

scalerModelOrg.mean


# In[19]:

scalerModelOrg.std


# In[20]:

featuresEnsuredDF = ensureDenseFeatures(featurizedDF)


# In[21]:

scalerModelEnsured = scaler.fit(featurizedDF)


# In[22]:

scalerModelEnsured.mean


# In[23]:

scalerModelEnsured.std


# In[24]:

featurizedScaledDF = scalerModelOrg.transform(featurizedDF)


# In[25]:

featurizedScaledDF.first()


# In[26]:

featurizedScaledEnsuredDF = scalerModelEnsured.transform(featuresEnsuredDF)


# In[27]:

featurizedScaledEnsuredDF.first()


# In[28]:

scalerModel = scalerModelEnsured
featurizedScaledDF = featurizedScaledEnsuredDF


# In[29]:

featurizedScaledEnsuredDF.columns


# In[30]:

scaledFeaturesRdd = (
    featurizedScaledEnsuredDF.select("scaled_features").rdd 
    .map(lambda r: r[0])
)


# In[31]:

k = 3
kmeansModelMllib = mllib.clustering.KMeans.train(scaledFeaturesRdd, k=k)


# In[32]:

kmeansModelMllib.centers


# In[33]:

kmeansModelMllib.computeCost(scaledFeaturesRdd)


# In[34]:

x = scaledFeaturesRdd.first()
x


# In[35]:

kmeansModelMllib.predict(x)


# In[36]:

x - kmeansModelMllib.centers[1]


# In[37]:

help(x.norm)


# In[38]:

(x - kmeansModelMllib.centers[1]).norm(2)


# In[39]:

def predictKMeans(x, centers):
    return min(
        (i for i in range(len(centers))), 
        key=lambda i: (x - centers[i]).norm(2))


# In[40]:

predictKMeans(x, kmeansModelMllib.centers)


# In[41]:

firstRow = featurizedScaledEnsuredDF.first()


# In[42]:

def addKMeansPredictionToDataFrame(df, featuresCol, predictionCol, kMeansModelCenters):
    return (
        df.rdd
        .map(lambda r: addKMeansPredictionToRow(r, featuresCol, kMeansModelCenters))
        .toDF(df.columns + [predictionCol])
    )

def addKMeansPredictionToRow(row, featuresCol, kMeansModelCenters):
    features = row.asDict()[featuresCol]
    return row + (predictKMeans(features, kMeansModelCenters),)


# In[43]:

mllibKmeansClusterScaledEnsuredDF = addKMeansPredictionToDataFrame(
    featurizedScaledEnsuredDF, 
    "scaled_features", 
    "pred_kmeans_mllib", 
    kmeansModelMllib.centers)


# In[44]:

(
    mllibKmeansClusterScaledEnsuredDF.select(["pred_kmeans_mllib", "churn"])
    .registerTempTable("tmp_kmeans_clustering_results")
)


# In[45]:

sqlContext.sql("""
SELECT 
    pred_kmeans_mllib
    , COUNT(*) AS n_obs
    , AVG(churn) as class_balance
FROM tmp_kmeans_clustering_results
GROUP BY pred_kmeans_mllib
ORDER BY n_obs DESC
""").toPandas()


# In[46]:

ks = list(range(1, 10))


# In[47]:

kMllibModelPairs = [
    (k, mllib.clustering.KMeans.train(scaledFeaturesRdd, k=k)) 
    for k in ks
]


# In[48]:

kMllibCostPairs = [
    (k, model.computeCost(scaledFeaturesRdd)) 
    for k, model in kMllibModelPairs
]


# In[49]:

kMllibCostPairs


# In[50]:

kMllibCostPdDf = pd.DataFrame.from_records(
    kMllibCostPairs, 
    columns=["k", "cost"])


# In[51]:

kMllibCostPdDf


# In[52]:

gg.ggplot(kMllibCostPdDf, gg.aes(x="k", y="cost")) + gg.geom_line()


# In[63]:

def computeFK(k, SSE, prevSSE, dim):
    if k == 1 or prevSSE == 0:
        return 1
    weight = weightFactor(k, dim)
    return SSE / (weight * prevSSE)

# calculating alpha_k in functional style with tail recursion -- which is not optimized in Python :(
def weightFactor(k, dim):
    if not k >= 2:
        raise ValueError("k should be greater than 1.")
        
    def weightFactorAccumulator(acc, k):
        if k == 2:
            return acc
        return weightFactorAccumulator(acc + (1 - acc) / 6, k - 1)
        
    k2Weight = 1 - 3 / (4 * dim)
    return weightFactorAccumulator(k2Weight, k)


# In[64]:

def computeFKs(ksAndSSEPairs, dimension):
    triples = makeFkTriples(ksAndSSEPairs)
    ksWithFks = [
        (k, computeFK(k, Sk, prevSk, dimension))
        for (k, Sk, prevSk) in triples]
    return sorted(ksWithFks, key=lambda pair: pair[0])


def makeFkTriples(ksAndSSEPairs):
    sortedPairs = sorted(ksAndSSEPairs, reverse=True)
    candidates = list(zip(sortedPairs, sortedPairs[1:]))
    triples = [
        (k, SSE, prevSSE)
        for ((k, SSE), (prevK, prevSSE)) in candidates
        if k - prevK == 1
    ]
    return triples


# In[65]:

import pandas as pd


# In[66]:

rows = [(1, 2511864.0000000005),
 (2, 2159268.216792464),
 (3, 2021512.0067232654),
 (4, 1737739.936959987),
 (5, 1622332.9551334414),
 (6, 1540179.6173300275),
 (7, 1496984.0873304429),
 (8, 1456057.3198812644),
 (9, 1384368.3444882743)]


# In[67]:

metricsPdDF = pd.DataFrame.from_records(rows, columns=["k", "SSE"])
metricsPdDF


# In[68]:

dimension = 2 

ksAndSSEPairs = list(map(tuple, metricsPdDF.to_records(index=False)))
ksAndSSEPairs


# In[69]:

ksWithFks = computeFKs(ksAndSSEPairs, dimension)
ksWithFks


# In[70]:

import ggplot as gg


# In[71]:

ksWithFksPdDF = pd.DataFrame.from_records(ksWithFks, columns=["K", "fK"])
ksWithFksPdDF


# In[72]:

gg.ggplot(gg.aes(x="K", y="fK"), data=ksWithFksPdDF) + gg.geom_line() + gg.ylab("f(K)")


# In[ ]:



