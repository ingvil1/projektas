
# coding: utf-8

# In[1]:

get_ipython().system('pip install findspark')


# In[2]:

import csv


# In[3]:

import pyspark


# In[4]:

import findspark
import os
import csv
from pyspark import ml
from pyspark import mllib
import pyspark.ml.feature
import pyspark.ml.clustering
import pyspark.ml.classification
import pyspark.ml.evaluation
import pyspark.mllib.feature
import pyspark.mllib.clustering
import ggplot as gg
import pandas as pd
import numpy as np


# In[5]:

get_ipython().magic('matplotlib inline')


# In[6]:

get_ipython().system(' pwd')


# In[7]:

get_ipython().system(' tree -L 2')


# In[8]:

findspark.init()


# In[9]:

os.makedirs(os.path.join(os.getcwd(), "output"), exist_ok=True)


# In[10]:

churnersDF = (
    sqlContext.read.format("com.databricks.spark.csv")
    .options(inferSchema=True, header=True)
    .load("customer_churners_00027.csv")
)


# In[11]:

usageDF = (
    sqlContext.read.format("com.databricks.spark.csv")
    .options(inferSchema=True, header=True)
    .load("customer_usage_00027.csv")
)


# In[12]:

churnersDF.columns


# In[13]:

usageDF.columns


# In[14]:

churnersDF


# In[15]:

usageDF


# In[16]:

final = usageDF.join(churnersDF,['user_account_id'],"outer")


# In[17]:

final.registerTempTable("customer_usage")


# In[18]:

dateCols = ["year","month"]


# In[19]:

idCols = ["user_account_id"]


# In[20]:

binaryCols = [
    "user_intake",
    "user_has_outgoing_calls", "user_has_outgoing_sms", 
    "user_use_gprs", "user_does_reload"
]


# In[21]:

categoricalCols = dateCols + binaryCols + idCols

continuousCols = [c for c in final.columns if c not in categoricalCols]


# In[22]:

sqlExprsMeanCols = ["AVG({0}) AS {0}".format(c) for c in continuousCols]
sqlExprsMeanCols


# In[23]:

sqlExprsBinaryCols = ["MAX({0}) AS {0}".format(c) for c in binaryCols]
sqlExprsBinaryCols


# In[24]:

sqlExprsDateCols = ["COUNT(*) AS n_months"]
sqlExprsDateCols


# In[25]:

sqlSelectExprs = sqlExprsMeanCols + sqlExprsBinaryCols + sqlExprsDateCols
sqlSelectExprs


# In[26]:

sqlSelect = ", ".join(sqlSelectExprs)
sqlSelect


# In[27]:

aggregatedfinal = sqlContext.sql("""
    SELECT user_account_id, {}
    FROM customer_usage
    GROUP BY user_account_id
""".format(sqlSelect))


# In[28]:

aggregatedfinal.first()


# In[29]:

get_ipython().system(' rm -rf output/aggregated_final/')


# In[30]:

aggregatedfinal.write.format("com.databricks.spark.csv").save("output/aggregated_final")


# In[31]:

get_ipython().system(' head -n 1 output/aggregated_final/part-00000')


# In[32]:

get_ipython().system(' ls output/aggregated_final/')


# In[33]:

with open("output/header__aggregated_final.txt", "w") as f:
    f.write(",".join(aggregatedfinal.columns))


# In[34]:

aggregatedfinalNoColumnsDF = (
    sqlContext.read.format("com.databricks.spark.csv")
    .options(inferSchema=True, header=False)
    .load("output/aggregated_final/")
)


# In[35]:

aggregatedfinalNoColumnsDF.rdd.first()


# In[36]:

with open("output/header__aggregated_final.txt") as f:
    columns = f.read().split(",")


# In[37]:

columns


# In[38]:

aggregatedfinal2 = (
    sqlContext.read.format("com.databricks.spark.csv")
    .options(inferSchema=True, header=False)
    .load("output/aggregated_final/")
    .rdd
    .toDF(columns)  
)


# In[39]:

aggregatedfinal2.first()


# In[40]:

with open("output/columns_continuous__agg_final.txt", "w") as f:
    f.write(",".join(continuousCols))


# In[41]:

with open("output/columns_binary__agg_final.txt", "w") as f:
    f.write(",".join(continuousCols))


# In[42]:

with open("output/columns_ids__agg_final.txt", "w") as f:
    f.write(",".join(idCols))


# In[43]:

with open("output/columns_misc__agg_final.txt", "w") as f:
    f.write("n_months")


# In[44]:

aggregatedfinal2.first()


# In[45]:

aggregatedfinal2


# In[46]:

aggregatedfinal2.toPandas().to_csv('merge.csv')


# In[47]:

summaryDF=aggregatedfinal2.describe()


# In[48]:

summaryDF.toPandas().T


# In[49]:

aggregatedfinal2.toPandas().head()


# In[50]:

c=aggregatedfinal2.toPandas()


# In[51]:

correlation=c.corr(method='pearson')


# In[52]:

correlation.to_excel('koreliacija.xlsx')


# In[53]:

import IPython
import math
from ggplot import *
get_ipython().magic('matplotlib inline')


# In[54]:

aggregatedfinal2 = pd.read_csv("merge.csv")


# In[55]:

inactive=aggregatedfinal2[aggregatedfinal2.churn == 1]


# In[56]:

active=aggregatedfinal2[aggregatedfinal2.churn == 0]


# In[57]:

inactive.describe().transpose()


# In[58]:

active.describe().transpose()


# In[59]:

g1=inactive[['user_no_outgoing_activity_in_days']]


# In[60]:

ggplot(aes(x='user_no_outgoing_activity_in_days'), data=g1) + geom_bar(binwidth=50)


# In[61]:

g2=active[['user_no_outgoing_activity_in_days']]


# In[62]:

ggplot(aes(x='user_no_outgoing_activity_in_days'), data=g2) + geom_bar(binwidth=50)


# In[63]:

g3=inactive[['user_lifetime']]


# In[64]:

ggplot(aes(x='user_lifetime'), data=g3) + geom_bar(binwidth=50)


# In[65]:

g4=active[['user_lifetime']]


# In[66]:

ggplot(aes(x='user_lifetime'), data=g4) + geom_bar(binwidth=50)

